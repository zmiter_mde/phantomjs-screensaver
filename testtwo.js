var pageFactory = require('webpage');
var page = pageFactory.create();

page.open('http://github.com', function(status) {
	console.log("finished loading github");
	if (status === 'success')
		page.render('github.png');
});

console.log("github opened");

setTimeout(function() {
	page.open('http://htmlbook.ru', function(status) {
		console.log("finished loading htmlbook");
		if (status === 'success')
			page.render('htmlbook.png');
	});

	console.log("htmlbook opened");	
}, 10000);

