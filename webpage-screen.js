var pageFactory = require('webpage');
var page = pageFactory.create();

page.viewportSize = {
  width: 1920,
  height: 2000
};

var pageURL = 'http://95.143.123.99:8080/WebDoc/?divid=1-40ARX&year=2015&period=1&vers=0&formid=SB0002&mode=e';
/*
page.onResourceTimeout = function(request) {
    console.log('Response (#' + request.id + '): ' + JSON.stringify(request));
};
page.onResourceError = function(resourceError) {
  console.log('Unable to load resource (#' + resourceError.id + 'URL:' + resourceError.url + ')');
  console.log('Error code: ' + resourceError.errorCode + '. Description: ' + resourceError.errorString);
};
page.onResourceRequested = function(requestData, networkRequest) {
  console.log('Request (#' + requestData.id + '): ' + JSON.stringify(requestData));
};
page.onResourceReceived = function(response) {
  console.log('Response (#' + response.id + ', stage "' + response.stage + '"): ' + JSON.stringify(response));
};
page.onPrompt = function(msg, defaultVal) {
	return 'PhantomJS';
};	
page.onLoadStarted = function() {
  var currentUrl = page.evaluate(function() {
    return window.location.href;
  });
  console.log('Current page ' + currentUrl + ' will gone...');
  console.log('Now loading a new page...');
};
page.onLoadFinished = function(status) {
  console.log('Status: ' + status);
  // Do other things here...
};
page.onAlert = function(msg) {
	console.log('ALERT: ' + msg);
};
page.onError = function(msg, trace) {

	var msgStack = ['ERROR: ' + msg];

	if (trace && trace.length) {
		msgStack.push('TRACE:');
		trace.forEach(function(t) {
			msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function +'")' : ''));
		});
	}

	console.error(msgStack.join('\n'));

};
phantom.onError = function(msg, trace) {
  var msgStack = ['PHANTOM ERROR: ' + msg];
  if (trace && trace.length) {
    msgStack.push('TRACE:');
    trace.forEach(function(t) {
      msgStack.push(' -> ' + (t.file || t.sourceURL) + ': ' + t.line + (t.function ? ' (in function ' + t.function +')' : ''));
    });
  }
  console.error(msgStack.join('\n'));
  phantom.exit(1);
};
*/
page.open(pageURL, function (status) {

	console.log("In page open");

	var pageReadyIntervalId, renderIntervalId, readyToRender = false, goLookForChildren = true;

	if (status === 'success') {
		console.log("Status success");
		pageReadyIntervalId = setInterval(tryToRender, 1000);	
	}

	function tryToRender() {
		console.log("In trying to render");
		if (page.evaluate(elementWithAttributeExistsOnLex)) {
			console.log("In if in tryToRender");
			clearInterval(pageReadyIntervalId);

			readyToRender = page.evaluate(isPageReadyToRender);
			setTimeout(function() {
				renderIntervalId = setInterval(render, 1000);	
			}, 10000);
			
		}
	}

	function elementWithAttributeExistsOnLex() {
		var matchingElements = [];
		var allElements = document.getElementsByTagName('div');
		for (var i = 0, n = allElements.length; i < n; i++) {
			if (allElements[i].getAttribute('role') == 'tree') {
				matchingElements.push(allElements[i]);
			}
		}
		return matchingElements.length > 0 ;		
	}

	function isPageReadyToRender() {
		console.log("In is page ready to render");
		// For Lex
		document.getElementsByClassName("v-splitpanel-first-container").item(0).style.width = '35%';
		console.log("Getting tree");
		var tree = document.getElementsByClassName("v-tree-node-root");

		var hasClass = function(element, cls) {
		    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
		}

		var exploreTree = function(elem) {
		    var children = elem.getElementsByClassName("v-tree-node");
		    var childrenRoot = elem.getElementsByClassName("v-tree-node-children");
		    if (children.length == 0 && childrenRoot.length > 0)
		        children = childrenRoot.item(0).getElementsByClassName("v-tree-node");
		    for (var i = children.length - 1; i >= 0; --i) {
		        if (!hasClass(children[i], "v-tree-node-expanded"))
		            children[i].click();
		        exploreTree(children[i]);
		    }
		}

		console.log("Exploring tree");
		exploreTree(tree[0]);		
/*
		var a = document.getElementsByClassName("v-filterselect-button");
		while (a.length > 0) 
		    a[0].parentNode.removeChild(a[0]);
*/		
		return true;
	}

	function render() {
		console.log("In render");
		
		if (readyToRender) {
			console.log("Ready to render");
			clearInterval(renderIntervalId);

			if (!goLookForChildren) {
				page.render('test.png');	
				console.log("Page rendered");
				phantom.exit();
			} 

			console.log("In if for looking for children");

			var count = 0, goNext = true;
			var links = page.evaluate(function() {
				return document.getElementsByClassName("v-tree-node");
				var res = [];
				var links = document.getElementsByClassName("v-tree-node");
				res.linksInfo = "Links count = " + links.length;
				for (var i = 0; i < links.length; ++i) {
					var children = links[i].getElementsByClassName("v-tree-node-children");
					if (children.length > 0) {
						if (children[0].innerHTML.length == 0)
							res.push(links[i]);
					}
				}
				return res;
			});

			console.log("Finished links evaluation");
	
			var goNextInterval = setInterval(function() {
				console.log("In going next");
				console.log("Links count = " + links.length);
				console.log(links.linksInfo);
				if (goNext && links.length > 0) {
					console.log("We're going next");
					goNext = false;
					var nextPageReady = page.evaluate(clickOnLink, count);
					console.log("Next page is ready " + nextPageReady);
					var nextPageRenderIntervalId = setInterval(function() {
						console.log("In next page ready");
						if (nextPageReady.length > 0) {
							clearInterval(nextPageRenderIntervalId);
							console.log(nextPageReady);
							page.render("test" + count + '.png');
							page.reload();
							goNext = true;
							++count;
						} else {
							console.log("Next page is not ready");
						}
					}, 1000);

					if (!goNext && count > links.length) {
						clearInterval(goNextInterval);
						console.log(links.linksInfo);
						console.log("Max count = " + links.length);
						console.log("Exiting!!!!!!!!");
						setTimeout(function() {
							phantom.exit();
						}, 50000);
					}
				} else {
					console.log("Not going further");
				}
				
			}, 10000);
		} else {
			console.log("Not ready to render");
		}
		
	}

	function clickOnLink(number) {
		var links = document.getElementsByClassName("v-tree-node");
		var res = [];
		for (var i = 0; i < links.length; ++i) {
			var children = links[i].getElementsByClassName("v-tree-node-children");
			if (children.length > 0) {
				if (children[0].innerHTML.length == 0)
					res.push(links[i]);
			}
		}
		var id = res[number].getAttribute("id");
		document.getElementById(id).click();
		return id;
	}
});