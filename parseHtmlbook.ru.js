var pageFactory = require('webpage');
var page = pageFactory.create();

var pageURL = 'http://github.com';

page.open(pageURL, function (status) {

	var pageReadyIntervalId, renderIntervalId, readyToRender = false, goLookForChildren = false;
	if (status === 'success') {
		pageReadyIntervalId = setInterval(tryToRender, 1000);	
	}

	function tryToRender() {
		if (page.evaluate(elementWithAttributeExistsOnHTMLBook)) {
			clearInterval(pageReadyIntervalId);

			readyToRender = page.evaluate(isPageReadyToRender);

			renderIntervalId = setInterval(render, 1000);
		}
	}

	function elementWithAttributeExistsOnHTMLBook() {
		return document.getElementsByClassName("container").length > 0;
	}

	function isPageReadyToRender() {
		var footers = document.getElementsByClassName("container");
		while (footers.length > 0)
			footers[0].parentNode.removeChild(footers[0]);
		return true;
	}

	function render() {
		if (readyToRender) {
			clearInterval(renderIntervalId);

			if (goLookForChildren) {

				var count = 0, goNext = true, links = page.evaluate(getLinksListHTMLBook, pageURL);
				var maxCount = links.length;

				var goNextInterval = setInterval(function() {
					if (goNext) {
						goNext = false;
						var nextPageReady = page.evaluate(clickOnLink, count);
						var nextPageRenderIntervalId = setInterval(function() {
							if (nextPageReady) {
								clearInterval(nextPageRenderIntervalId);
								page.render("test" + count + '.png');
								goNext = true;
								++count;
							} 
						}, 1000);

						if (!goNext && count > maxCount) {
							clearInterval(goNextInterval);
							phantom.exit();
						}
					}
				}, 1000);
			} else {
				//page.render('test' + count + '.pdf');
				captureSelector("test.png", ".container");
				//phantom.exit();	
			}
						
		}
	}

	function getLinksListHTMLBook(pageURL) {
		var res = [];
		var menu = document.getElementsByClassName("menu").item(0);
		var links = menu.getElementsByTagName("li");
		for (var i = 0; i < links.length; ++i)
			res[i] = pageURL + links[i].getElementsByTagName("a").item(0).getAttribute("href");
		return res;
	}

	

	function clickOnLink(number) {
		var links = document.getElementsByClassName("menu").item(0).getElementsByTagName("li");
		var href = links[number].getElementsByTagName("a").item(0);
		href.click();
		return true;
	}

	var capture = function(targetFile, clipRect) {
	    var previousClipRect;
	    var clipRect = {top: 0, left:0, width: 400, height: 400};
	    if (clipRect) {
	    	/*
	        if (!isType(clipRect, "object")) {
	            throw new Error("clipRect must be an Object instance.");
	        }
	        */
	       // previousClipRect = page.clipRect;
	        //page.clipRect = clipRect;
	        console.log('Capturing page to ' + targetFile + ' with clipRect' + JSON.stringify(clipRect), "debug");
	    } else {
	        console.log('Capturing page to ' + targetFile, "debug");
	    }
	    try {
	        page.render(targetFile);
	    } catch (e) {
	        console.log('Failed to capture screenshot as ' + targetFile + ': ' + e, "error");
	    }
	    if (previousClipRect) {
	        page.clipRect = previousClipRect;
	    }
	    return this;
	}

	var captureSelector = function(targetFile, selector) {
	    var selector = selector;
	    return capture(targetFile, page.evaluate(function(selector) {  
	        try { 
	            var clipRect = document.querySelector(selector).getBoundingClientRect();
	            return {
	                top: clipRect.top,
	                left: clipRect.left,
	                width: clipRect.width,
	                height: clipRect.height
	            };
	        } catch (e) {
	            console.log("Unable to fetch bounds for element " + selector, "warning");
	        }
	    }, { selector: selector }));
	}
	
});